package main

import (
	"github.com/veandco/go-sdl2/sdl"
)

var GlobalPlayer *PlayerObject
var enemy *PlayerObject
var gameMap *GameMap

// Global Platform Manager
var GlobalPlatformManager *PlatformManager

// GlobalMouse position and state

// GlobalRenderer Global Renderer for all game
var GlobalRenderer *sdl.Renderer

// Game Struct that represent the game setup
type Game struct {
	// IsRuning: Indicates when engine is runing
	IsRuning bool
	// Counter: Debug Purposes
	Counter int
	window  *sdl.Window
}

// NewGame Game Constructor
func NewGame() *Game {
	return &Game{
		IsRuning: false,
		Counter:  0,
		window:   nil,
	}
}

// Init Initialize SDL
func (g *Game) Init(title string, xpos, ypos, width, height int32, fullscreen bool) {
	var flags uint32 = 0
	if fullscreen {
		flags = sdl.WINDOW_FULLSCREEN
	}
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		ErrorPrint.Println("Cannot initialize SDL")
		g.IsRuning = false
		return
	}
	InfoPrint.Println("SDL SubSystems Initialized")
	window, err := sdl.CreateWindow(title, xpos, ypos, width, height, flags)
	if err != nil {
		ErrorPrint.Printf("Can't create the window, %v \n", err)
		g.IsRuning = false
		return
	}
	InfoPrint.Println("Window has been created Sucesfully.")

	renderer, err := sdl.CreateRenderer(window, -1, 0)
	if err != nil {
		ErrorPrint.Printf("Cant create the renderer. %v \n", err)
		g.IsRuning = false
		return
	}
	InfoPrint.Println("Renderer has been created Sucesfully.")
	renderer.SetDrawColor(255, 255, 255, 255)
	g.IsRuning = true
	g.window = window
	GlobalRenderer = renderer

	GlobalPlayer = NewGameObject("res/img/platform-2.png", 0, 0, 4, 4)
	InfoPrint.Println("GlobalPlayer initialized")

	// enemy = NewGameObject("res/img/enemy.png", 100, 100, 2)
	gameMap = NewMap()
	//InfoPrint.Println("Platforms :D : ", len(platforms))

	// Global Platform Manager Initialization
	GlobalPlatformManager = NewPlatformManager()
	InfoPrint.Println("Global platform manager initialized")
}

// HandleEvents Game Event Loop
func (g *Game) HandleEvents() {
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			InfoPrint.Println("Quit event handled")
			g.IsRuning = false
			return
		case *sdl.KeyboardEvent:
			GlobalPlayer.Step(t.Keysym.Sym, t.Type)
		default:
			return
		}
	}

}

// Update update the game
func (g *Game) Update() {
	GlobalPlayer.Update()
}

// Render render the game into the window
func (g *Game) Render() {
	GlobalRenderer.Clear()
	gameMap.DrawTiles()
	GlobalPlatformManager.DrawPlatforms()
	GlobalPlayer.Render()
	GlobalRenderer.Present()
}

// Clean the window, renderer and stops SDL
func (g *Game) Clean() {
	g.window.Destroy()
	GlobalRenderer.Destroy()
	sdl.Quit()
	InfoPrint.Println("Game Cleaned")
}
