package main

import (
	"github.com/fatih/color"
	"github.com/veandco/go-sdl2/sdl"
)

const fps int = 60
const frameDelay int = 1000 / fps

var (
	// InfoPrint is for information print
	InfoPrint = color.New(color.FgGreen)
	// ErrorPrint is for error print
	ErrorPrint = color.New(color.FgRed)
	// WarningPrint is for warning print
	WarningPrint = color.New(color.FgYellow)

	frameStart uint32 = 0
	frameTime  int    = 0
)

var game *Game

func main() {
	// IGNORE
	tg := NewTextureGetter()
	tg.LoadTextureData()
	game = NewGame()
	game.Init("Game Engine Demo", sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED, 800, 640, false)
	for {
		if !game.IsRuning {
			break
		}
		frameStart = sdl.GetTicks()

		game.HandleEvents()
		game.Update()
		game.Render()

		frameTime = int(sdl.GetTicks() - frameStart)

		if frameDelay > frameTime {
			sdl.Delay(uint32(frameDelay - frameTime))
		}

	}

	game.Clean()
}
