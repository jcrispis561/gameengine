package main

import (
	"github.com/veandco/go-sdl2/sdl"
)

// GameMap holds the global map textures and the level information
type GameMap struct {
	water, dirt, grass *sdl.Texture
	levelMap           [20][25]uint
	levelTiles         []*Platform
}

// The Texture Mapping Holds all texture information
var GlobalTextureMapping *TextureMapping

// This is only for testing purposes
var Lvl1 [20][25]uint = [20][25]uint{
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1},
	{2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 2, 2, 2},
	{2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 2, 2, 2},
}

// NewMap returns a new GameMap instance
func NewMap() *GameMap {
	dirtTexture := LoadTexture("res/img/dirt.png")
	grassTexture := LoadTexture("res/img/grass.png")
	waterTexture := LoadTexture("res/img/water.png")

	// Map texture Data
	gm := &GameMap{
		water: waterTexture,
		dirt:  dirtTexture,
		grass: grassTexture,
	}

	gm.LoadMap(Lvl1)
	GlobalTextureMapping = NewTextureGetter()
	return gm
}

// LoadMap loads a map into the memory
func (m *GameMap) LoadMap(levelArray [20][25]uint) {
	for iRow := 0; iRow < 20; iRow++ {
		for iCol := 0; iCol < 25; iCol++ {
			m.levelMap[iRow][iCol] = levelArray[iRow][iCol]
		}
	}
	InfoPrint.Println("Level loaded in RAM")
}

// DrawTiles creates a slice Platform types
func (m *GameMap) DrawTiles() {
	source := &sdl.Rect{X: 0, Y: 0, W: 32, H: 32}
	for iRow := 0; iRow < 20; iRow++ {
		for iCol := 0; iCol < 25; iCol++ {
			dest := &sdl.Rect{X: int32(iCol * 32), Y: int32(iRow * 32), W: 32, H: 32}

			dest.X = int32(iCol * 32)
			dest.Y = int32(iRow * 32)

			switch m.levelMap[iRow][iCol] {
			case 0:
				DrawTexture(m.water, source, dest)
			case 1:
				DrawTexture(m.grass, source, dest)
			case 2:
				DrawTexture(m.dirt, source, dest)
			}
		}
	}
}
