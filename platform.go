package main

import (
	"encoding/json"
	"io/ioutil"

	"github.com/veandco/go-sdl2/sdl"
)

const (
	texturePropsPath string = "res/data/texture_props.json"
)

var debugTexturePath [2]string = [2]string{"res/img/platform-2.png", "res/img/platform-3.png"}

// Platform structure
type Platform struct {
	id            uint
	dest          *sdl.Rect
	debugTextures [2]*sdl.Texture
	isSolid       bool
	showBounding  bool
	IsActive      bool
}

type ColissionObject struct {
	colided bool
	V, H    bool
	value   int32
}

// Platforms collection
type Platforms []*Platform

// TextureProps Holds json texture information in resources folder
type TextureProps struct {
	Id      uint   `json:"id"`
	Name    string `json:"name"`
	IsFloor bool   `json:"isFloor"`
}

// TextureDescriptors Hold the descriptors of a texture
type TextureDescriptors struct {
	name    string
	isFloor bool
}

// TextureMapping is a hashmap for index textures to ids
type TextureMapping map[uint]TextureDescriptors

// Hardcoded Texture Floor Floor temporally

// NewTextureGetter TextureMapping Constructor
func NewTextureGetter() *TextureMapping {
	var tg TextureMapping = make(map[uint]TextureDescriptors)
	tg.LoadTextureData()
	return &tg
}

// LoadTextureData loads the game data
func (t *TextureMapping) LoadTextureData() {
	var jsonContainer []TextureProps

	byteValue, _ := ioutil.ReadFile(texturePropsPath)
	if err := json.Unmarshal(byteValue, &jsonContainer); err != nil {
		ErrorPrint.Printf("Cannot open unmarshall: %v\n", err)
	}

	for _, obj := range jsonContainer {
		(*t)[obj.Id] = TextureDescriptors{
			name:    obj.Name,
			isFloor: obj.IsFloor,
		}
	}
}

// GetID return the properties of an object
func (t *TextureMapping) GetID(id uint) TextureDescriptors {
	return (*t)[id]
}

// NewPlatform constructs a new Platform
func NewPlatform(dest *sdl.Rect, isSolid, showBounding bool, id uint) *Platform {

	return &Platform{
		dest:         dest,
		isSolid:      isSolid,
		showBounding: showBounding,
		id:           id,
	}
}

// DrawPlatform Draws all the platforms
func (p Platform) DrawPlatform() {
	var ct *sdl.Texture

	if p.IsActive {
		ct = p.debugTextures[0]
	} else {
		ct = p.debugTextures[1]
	}

	r := &sdl.Rect{X: 0, Y: 0, W: 32, H: 32}
	DrawTexture(ct, r, p.dest)

}

// TODO: Platform Detection Algorithm

// DetectFloor floor Detection Algorithm
func DetectFloor(levelData [20][25]uint, showBounding, isSolid bool) Platforms {
	x := -1
	y := 0
	w := 0
	h := 32
	tmy := 0
	tmx := 0
	// Debug Textures
	var dt [2]*sdl.Texture

	if showBounding {
		for i, path := range debugTexturePath {
			dt[i] = LoadTexture(path)
		}
	}
	// end Debug
	var p Platforms
	var id_assign uint
	for c := 0; c < 20; c++ {
		for r := 0; r < 25; r++ {
			tmy = c * 32 // Current tile position Y
			tmx = r * 32 // Current tile position X

			// Floor is horizontal only
			textureProperties := GlobalTextureMapping.GetID(uint(levelData[c][r]))
			if textureProperties.isFloor {
				w += 32
				y = tmy
				if x == -1 {
					x = tmx
				}
			} else if w > 0 {
				dest := sdl.Rect{X: int32(x), Y: int32(y), W: int32(w), H: int32(h)}
				np := NewPlatform(&dest, isSolid, showBounding, id_assign)
				id_assign++
				np.debugTextures = dt
				p = append(p, np)
				w = 0
				y = 0
				x = -1
			}
		}
	}
	return p
}

// Colission Very Basic way to detect Colision
func (p Platform) Colission(collider *sdl.Rect) ColissionObject {

	return p.ColissionDetection(collider)
}

// ColissionDetection More advanced box collision detection
func (p Platform) ColissionDetection(collider *sdl.Rect) ColissionObject {
	res := ColissionObject{false, false, false, 0}

	// Calculate the 4 sides of rectangle A
	//rightA := p.dest.X + p.dest.W
	//bottomA := p.dest.Y + p.dest.H
	//leftA := p.dest.X
	topA := p.dest.Y

	// Calculate the 4 sides of rectangle A
	//rightB := collider.X + collider.W
	bottomB := collider.Y + collider.H
	//leftB := collider.X
	//topB := collider.Y
	if p.dest.HasIntersection(collider) {
		if bottomB >= topA {
			res.colided = true
			res.V = true
			res.value = p.dest.Y - collider.W + 1
			return res
		}
	}

	/*if rightB >= leftA {
		res.colided = true
		res.X = rightB
		return res
	}*/

	return res

}
