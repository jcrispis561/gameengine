package main

import (
	"github.com/veandco/go-sdl2/sdl"
)

// PlatformManager Platform to colide detection logic
type PlatformManager struct {
	currentPlatform *Platform
	platforms       Platforms
}

// NewPlatformManager Platform Manager Constructor
func NewPlatformManager() *PlatformManager {
	pltfs := DetectFloor(Lvl1, true, true)
	InfoPrint.Printf("%v Platforms created", len(pltfs))
	return &PlatformManager{
		currentPlatform: nil,
		platforms:       pltfs,
	}
}

// DrawPlatforms Draws all the platforms
func (p *PlatformManager) DrawPlatforms() {
	for _, platform := range p.platforms {
		if GlobalPlayer.destRect.X >= platform.dest.X && GlobalPlayer.destRect.X <= (platform.dest.W+platform.dest.X) && GlobalPlayer.ypos <= (platform.dest.Y) {
			p.currentPlatform = platform
			p.currentPlatform.IsActive = true
		} else {
			if p.currentPlatform != nil {
				platform.IsActive = false
			}
		}
		platform.DrawPlatform()
	}
}

// Colission Manager Platform Colission
func (p *PlatformManager) DetectColission(collider *sdl.Rect) ColissionObject {
	if p.currentPlatform != nil {
		colObject := p.currentPlatform.Colission(collider)
		return colObject
	}
	return ColissionObject{false, false, false, 0}
}
