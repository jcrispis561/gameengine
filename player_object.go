package main

import (
	"github.com/veandco/go-sdl2/sdl"
)

// PlayerObject represents an object in the game
type PlayerObject struct {
	xpos              int32
	ypos              int32
	objectTexture     *sdl.Texture
	srcRect, destRect *sdl.Rect
	vspeed, hspeed    int32
	controller        PlayerController
}

type PlayerController struct {
	up, down, left, right bool
}

// NewGameObject Game Object Constructor
func NewGameObject(textureSheet string, x, y int32, vspeed, hspeed int32) *PlayerObject {
	objectTexture := LoadTexture(textureSheet)
	pc := PlayerController{false, false, false, false}
	return &PlayerObject{
		xpos:          x,
		ypos:          y,
		objectTexture: objectTexture,
		srcRect:       nil,
		destRect:      nil,
		vspeed:        4,
		hspeed:        4,
		controller:    pc,
	}
}

// Update is the object update function
func (g *PlayerObject) Update() {
	// Gravity
	g.ypos += g.vspeed

	// Movement

	if g.controller.left {
		g.MoveLeft()
	}

	if g.controller.right {
		g.MoveRight()
	}

	sourceRectangle := &sdl.Rect{X: 0, Y: 0, H: 32, W: 32}
	destRectangle := &sdl.Rect{
		X: g.xpos,
		Y: g.ypos,
		H: 32,
		W: 32,
	}

	g.srcRect = sourceRectangle
	g.destRect = destRectangle

	g.OnCollision()

}

// Render is the function to render an object
func (g *PlayerObject) Render() {

	DrawTexture(g.objectTexture, g.srcRect, g.destRect)

}

// MoveRight Moves the player to the right
func (g *PlayerObject) MoveRight() {
	g.xpos += g.hspeed
}

// MoveLeft Moves the player to the left}
func (g *PlayerObject) MoveLeft() {
	g.xpos -= g.hspeed
}

// OnCollision when has one
func (g *PlayerObject) OnCollision() {
	res := GlobalPlatformManager.DetectColission(g.destRect)
	if res.colided {
		if res.V {
			g.vspeed = 0
			g.ypos = res.value
			return
		} else if res.H {
			g.hspeed = 0
			g.xpos = res.value
			return
		}
	} else {
		if g.vspeed == 0 {
			g.vspeed = 4
		}
	}
}

// Step manages the keys
func (g *PlayerObject) Step(k sdl.Keycode, t uint32) {
	if t == sdl.KEYDOWN {
		switch sdl.GetKeyName(k) {
		case "A":
			g.controller.left = true
			return

		case "D":
			g.controller.right = true
			return
		}
	}
	if t == sdl.KEYUP {
		switch sdl.GetKeyName(k) {
		case "A":
			g.controller.left = false
		case "D":
			g.controller.right = false
		}
	}

}
