# 2D simple platformer engine

A simple 2D platform game engine written in LibSDL2 C++ through golang sdl2 bindings, actually is WIP, but dispite of that the engine has a good and carefull progress.

## For Users

Actually has no fully compiled version but you can use the 2DGameEngine compiled in the repo to excecute the game engine

~~~bash
git clone https://gitlab.com/jcrispis561/gameengine.git
cd gameengine
./2DGameEngine
~~~

__ONLY LINUX COMPATIBILITY FOR NOW__

## For Developers

### Actual Engine Capabilities (Building Blocks Description)

Runs with OpenGL backend thereby you can use the hardware GPU capabilities for rendering the tiles in the screen.

#### 1. The player object
Has a player object capable of be self aware of the control signals because inside the struct itself have the controller, and the driver for that is the step function inside the, besides that the player object has all the necesary properties to modify the behavior in the screen

##### Properties
> __xpos, ypos__: Holds the X and y position where the renderer will draw it into the screen

> __objectTexture__: Holds the texture of the Player Object

> __src, dest__: Src Holds the .png properties and dst is the position and drawing position and dimensions

> __vspeed, hspeed__: The ammout of speed for a player

> __controller__: Is the struct capable of translate the keydowns to instructions for the position of the player
~~~go
type PlayerObject struct {
	xpos              int32
	ypos              int32
	objectTexture     *sdl.Texture
	srcRect, destRect *sdl.Rect
	vspeed, hspeed    int32
	controller        PlayerController
}
~~~

The player controller is only an object with 4 booleans that indicates where move the player

~~~go
type PlayerController struct {
	up, down, left, right bool
}
~~~

##### Methods and Functions

> __NewGameObject__: __Refactor Required__ is the constructor for a new player object

> __Update__: The update method for the player, this method apply all the changes to the player properties (Position, Movement)

> __Render__: All the pixel by pixel transformations and the dispatch of the texture to GPU is made here :D 

> __MoveLeft__, MoveRight: The movement of the player

> __OnCollision__: Manages the collision of the player

> __Step__: Manages the keyCodes and dispach the action to the controller


#### 2. The PlatFormManager

Personally my favorite, this object has the responsability of activate the platform colssion algorithm, only where the player of solid kinematic object is above of that this detects colission, otherwise the colission detection isn't trigered, also has a basic floor detection algorithm that reads the texture_props file to know if a tile is floor to strech the colission platform, with that we get have a lot of less colission blocks working simultaneously, and the CPU has less work.

__TODO__: Handle multiple platforms

~~~go
type PlatformManager struct {
	currentPlatform *Platform
	platforms       Platforms
}
~~~

---
![Platform Manager Platform Detection](doc/PDemo.gif)

##### Methods and Functions

> __NewPlatformManager__: is the constructor for the platform manager object

> __DrawPlatforms__: Collision Manager Debug Mode

> __DetectColission__: Collision Detection with the player

#### 3. Texture Manager

All the rendering, and visual behavior, image loading, image copy to GPU Memory is made here

#### Functions

> __LoadTexture__: Load a surface into the RAM memory and converts him to textures

> __DrawTexture__: Load the texture in the GPU memory and draws him

#### 4. Map

All Map info, tile positioning, tile drawing, and visual interpretation has made here, that use an algorithm that iterates into a map array __actually hardcoded__ and assigns them a position in the screen, holds the tile texture path info

~~~go
type GameMap struct {
	water, dirt, grass *sdl.Texture
	levelMap           [20][25]uint
	levelTiles         []*Platform
}
~~~

##### Methods and Functions

> __NewMap__: Constructor for a new game map 

> __LoadMap__: Interprets the numbers in the array

> __DrawMap__: Draw the map tiles into the screen


#### 5. Game

All initialization, event dispatching depends of this object, is the main driver, the initialization has multiple phases, 

- Reserves RAM Memory for all Global objects in the engine such as Player, Enemies, Game Map, PlatformManager, The renderer, all of this values are singletons

- Initialize the SDL Subsystem

- Creates the window object and assigns to him all the variable properties 

- Creates the global renderer

- And creates the minor hierarchy object (User Classes)

The event loop is defined here, this listen for all the events to distpatch them to target objects, such as KeyBoardEvent witch call the player step function

The update, holds the update function to all active object.

Render, is the function that excecutes all the render methods for the differents objects

~~~go
// Game Struct that represent the game setup
type Game struct {
	// IsRuning: Indicates when engine is runing
	IsRuning bool
	// Counter: Debug Purposes
	Counter int
	window  *sdl.Window
}
~~~

##### Methods and Functions

> __NewGame__: Returns a new Game instance

> __Init__: The initialization process

> __HandleEvents__: The event loop (__TODO__: Wrap into a go routine)

> __Update__: The update function for all the classes

> __Render__: The render function for all the active objects

> __Clean__: The function that excecutes on Quit