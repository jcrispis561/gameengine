package main

import (
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

// LoadTexture loads a texture from a filepath
func LoadTexture(filename string) *sdl.Texture {
	tempSurface, err := img.Load(filename)
	defer tempSurface.Free()
	if err != nil {
		ErrorPrint.Printf("Failed to create the texture: %v\n", err)
	}

	texture, err := GlobalRenderer.CreateTextureFromSurface(tempSurface)
	if err != nil {
		ErrorPrint.Printf("Failed to create the texture: %v\n", err)
	}
	InfoPrint.Printf("Resource %v loaded.\n", filename)

	return texture
}

// DrawTexture takes a texture and Draws into src and dst positions in the screen
func DrawTexture(tex *sdl.Texture, src, dest *sdl.Rect) {
	GlobalRenderer.Copy(tex, src, dest)
}
